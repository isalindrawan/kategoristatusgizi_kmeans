package kategoristatusgizi;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class ecludean_distance {

    private ArrayList<datamodel> dataset;
    private ArrayList<Double> temp;
    private ArrayList<ArrayList<Double>> result, centroid;
    private Double usia, berat, tinggi, usia2, berat2, tinggi2, hasil;

    public ecludean_distance(ArrayList<datamodel> dataset, ArrayList<ArrayList<Double>> centroid) {

        this.dataset = dataset;
        this.centroid = centroid;

        ecludean();
    }

    private void ecludean() {

        result = new ArrayList<ArrayList<Double>>();

        //pengulangan sebanyak besarnya data
        for (int i = 0; i < centroid.size(); i++) {

            //menyimpan variabel usia, berat, tinggi yang akan di operasikan
            usia = centroid.get(i).get(0);
            berat = centroid.get(i).get(1);
            tinggi = centroid.get(i).get(2);

            //inisiasi variabel penyimpan data sementara
            temp = new ArrayList<Double>();

            //pengulangan sebanyak besarnya data
            for (int j = 0; j < dataset.size(); j++) {

                //mengambil data yang akan dioperasikan sebagai variabel ke dua
                usia2 = dataset.get(j).usia();
                berat2 = dataset.get(j).berat();
                tinggi2 = dataset.get(j).tinggi();

                //operasi perhitungan
                hasil = BigDecimal.valueOf(Math.sqrt(Math.pow(usia2 - usia, 2) + Math.pow(berat2 - berat, 2) + Math.pow(tinggi2 - tinggi, 2))).setScale(5, RoundingMode.HALF_UP).doubleValue();

                temp.add(hasil);
            }

            result.add(temp);
        }
    }

    public ArrayList<ArrayList<Double>> result() {

        return result;
    }
}
