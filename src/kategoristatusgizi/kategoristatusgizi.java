package kategoristatusgizi;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

public class kategoristatusgizi extends javax.swing.JFrame {

    public kategoristatusgizi() {
        initComponents();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        label_data_mentah = new javax.swing.JLabel();
        label_data_normalisasi = new javax.swing.JLabel();
        buttonGroup = new javax.swing.ButtonGroup();
        bukaPanel = new javax.swing.JPanel();
        bukaButton = new javax.swing.JButton();
        lokasiField = new javax.swing.JTextField();
        bukaLabel = new javax.swing.JLabel();
        tabContainer = new javax.swing.JTabbedPane();
        mentahScroller = new javax.swing.JScrollPane();
        mentah_Tabel = new javax.swing.JTable();
        normalScroller = new javax.swing.JScrollPane();
        normal_Tabel = new javax.swing.JTable();
        linkageScroller = new javax.swing.JScrollPane();
        linkage_Tabel = new javax.swing.JTable();
        mulaiButton = new javax.swing.JButton();
        sep1 = new javax.swing.JSeparator();
        jumlahPanel = new javax.swing.JPanel();
        jumlahLabel = new javax.swing.JLabel();
        radio3 = new javax.swing.JRadioButton();
        radio4 = new javax.swing.JRadioButton();
        radio5 = new javax.swing.JRadioButton();
        radio6 = new javax.swing.JRadioButton();
        radio2 = new javax.swing.JRadioButton();
        sep2 = new javax.swing.JSeparator();
        sep3 = new javax.swing.JSeparator();
        sep4 = new javax.swing.JSeparator();

        label_data_mentah.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        label_data_mentah.setText("Tabel Data Mentah");

        label_data_normalisasi.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        label_data_normalisasi.setText("Tabel Data Normalisasi");

        buttonGroup.add(radio3);
        buttonGroup.add(radio4);
        buttonGroup.add(radio5);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Kategori Status Gizi");
        setName("mainFrame"); // NOI18N

        bukaButton.setText("...");
        bukaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bukaButtonActionPerformed(evt);
            }
        });

        lokasiField.setEditable(false);

        bukaLabel.setText("Buka file :");

        javax.swing.GroupLayout bukaPanelLayout = new javax.swing.GroupLayout(bukaPanel);
        bukaPanel.setLayout(bukaPanelLayout);
        bukaPanelLayout.setHorizontalGroup(
            bukaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bukaPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bukaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bukaPanelLayout.createSequentialGroup()
                        .addComponent(lokasiField)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bukaButton))
                    .addGroup(bukaPanelLayout.createSequentialGroup()
                        .addComponent(bukaLabel)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        bukaPanelLayout.setVerticalGroup(
            bukaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bukaPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bukaLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bukaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bukaButton)
                    .addComponent(lokasiField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mentah_Tabel.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nama", "Usia", "Berat", "Tinggi", "Kategori"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        mentahScroller.setViewportView(mentah_Tabel);
        if (mentah_Tabel.getColumnModel().getColumnCount() > 0) {
            mentah_Tabel.getColumnModel().getColumn(4).setHeaderValue("Kategori");
        }

        tabContainer.addTab("Data", mentahScroller);

        normal_Tabel.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nama", "Usia", "Berat", "Tinggi"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        normalScroller.setViewportView(normal_Tabel);
        if (normal_Tabel.getColumnModel().getColumnCount() > 0) {
            normal_Tabel.getColumnModel().getColumn(0).setResizable(false);
            normal_Tabel.getColumnModel().getColumn(1).setResizable(false);
            normal_Tabel.getColumnModel().getColumn(2).setResizable(false);
            normal_Tabel.getColumnModel().getColumn(3).setResizable(false);
        }

        tabContainer.addTab("Normalisasi", normalScroller);

        linkage_Tabel.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "", "", "", "", ""
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        linkageScroller.setViewportView(linkage_Tabel);
        if (linkage_Tabel.getColumnModel().getColumnCount() > 0) {
            linkage_Tabel.getColumnModel().getColumn(0).setResizable(false);
            linkage_Tabel.getColumnModel().getColumn(1).setResizable(false);
            linkage_Tabel.getColumnModel().getColumn(2).setResizable(false);
            linkage_Tabel.getColumnModel().getColumn(3).setResizable(false);
            linkage_Tabel.getColumnModel().getColumn(4).setResizable(false);
        }

        tabContainer.addTab("Detail", linkageScroller);

        mulaiButton.setText("Mulai");
        mulaiButton.setEnabled(false);
        mulaiButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mulaiButtonActionPerformed(evt);
            }
        });

        sep1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jumlahLabel.setText("Jumlah (k) :");

        buttonGroup.add(radio3);
        radio3.setSelected(true);
        radio3.setText("3");
        radio3.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radio3ItemStateChanged(evt);
            }
        });

        buttonGroup.add(radio4);
        radio4.setText("4");
        radio4.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radio4ItemStateChanged(evt);
            }
        });

        buttonGroup.add(radio5);
        radio5.setText("5");
        radio5.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radio5ItemStateChanged(evt);
            }
        });

        buttonGroup.add(radio6);
        radio6.setText("6");
        radio6.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radio6ItemStateChanged(evt);
            }
        });

        buttonGroup.add(radio2);
        radio2.setText("2");
        radio2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radio2ItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jumlahPanelLayout = new javax.swing.GroupLayout(jumlahPanel);
        jumlahPanel.setLayout(jumlahPanelLayout);
        jumlahPanelLayout.setHorizontalGroup(
            jumlahPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jumlahPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jumlahPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jumlahPanelLayout.createSequentialGroup()
                        .addComponent(radio2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(radio3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(radio4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(radio5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(radio6))
                    .addComponent(jumlahLabel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jumlahPanelLayout.setVerticalGroup(
            jumlahPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jumlahPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jumlahLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jumlahPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radio3)
                    .addComponent(radio4)
                    .addComponent(radio5)
                    .addComponent(radio6)
                    .addComponent(radio2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        sep3.setOrientation(javax.swing.SwingConstants.VERTICAL);

        sep4.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sep2)
                    .addComponent(tabContainer, javax.swing.GroupLayout.DEFAULT_SIZE, 834, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(bukaPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sep1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jumlahPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sep3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mulaiButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sep4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jumlahPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(bukaPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sep1)
                    .addComponent(mulaiButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sep3)
                    .addComponent(sep4, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sep2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tabContainer, javax.swing.GroupLayout.DEFAULT_SIZE, 413, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    //action ketika tombol buka di tekan
    private void bukaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bukaButtonActionPerformed

        //inisiasi JFileChooser untuk jendela open file
        JFileChooser pilih = new javax.swing.JFileChooser();
        //inisiasi file filter untuk tipe file tertentu
        FileNameExtensionFilter filter = new FileNameExtensionFilter("csv files", "csv");
        //menambahkan file filter untuk hanya menampilkan file csv
        pilih.addChoosableFileFilter(filter);
        pilih.setFileSelectionMode(javax.swing.JFileChooser.FILES_ONLY);

        //membuka jendela open file
        int returnValue = pilih.showOpenDialog(null);

        //melakukan pengecekan ketika jendela open file tertutup apakah open file atau cancel
        if (returnValue == javax.swing.JFileChooser.APPROVE_OPTION) {

            //mendapatkan lokasi file yang dibuka beserta nama dan tipe file
            file = new java.io.File(pilih.getSelectedFile().getAbsolutePath());

            //melakukan pengecekan tipe data csv apakah benar csv atau tidak
            if (pilih.getSelectedFile().getAbsolutePath().contains("csv")) {

                lokasiField.setText(pilih.getSelectedFile().getAbsolutePath());

                mulaiButton.setEnabled(true);

            } else {
                //menampilkan pesan error ketika file yang dibuka bukan bertipe csv
                javax.swing.JOptionPane.showMessageDialog(null, "File tipe tidak valid, program hanya memproses file csv");
            }

        }
    }//GEN-LAST:event_bukaButtonActionPerformed

    //action ketika tombol mulai di tekan
    private void mulaiButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mulaiButtonActionPerformed

        hasil_SSE = new ArrayList<Double>();
        K_centroid = new ArrayList<ArrayList<Double>>();

        konversi();

        normalisasi_data();

        for (int i = 2; i < 7; i++) {

            centroid = new ArrayList<ArrayList<Double>>();

            if (i == cluster) {

                generate_centroid(i);

                hasil_SSE.add(kmeans(true, i));

            } else {

                generate_centroid(i);
                hasil_SSE.add(kmeans(false, i));
            }
        }

        centroid_ecludean = new ecludean_centroid(K_centroid_baru);
        jarak_centroid = centroid_ecludean.average_res();

        update_data();

        update_tabel_data();

        update_tabel_normal();

        update_tabel_centroid();

        update_tabel_sse();

        gambar_chart();

    }//GEN-LAST:event_mulaiButtonActionPerformed

    private void konversi() {

        //memanggil method konversi
        konversi ubah = new konversi(file);

        dataset = ubah.hasil_konversi();
    }

    private void normalisasi_data() {

        //memanggil method normalisasi
        normalisasi normal = new normalisasi(dataset);

        dataset_normal = normal.hasil_normalisasi();
    }

    private void generate_centroid(int k) {

//        if (k == cluster) {
//
//            centroid.clear();
//
//            ArrayList<Double> child;
//
//            child = new ArrayList<Double>();
//
//            child.add(dataset_normal.get(0).usia());
//            child.add(dataset_normal.get(0).berat());
//            child.add(dataset_normal.get(0).tinggi());
//
//            centroid.add(child);
//            K_centroid.add(child);
//
//            child = new ArrayList<Double>();
//
//            child.add(dataset_normal.get(1).usia());
//            child.add(dataset_normal.get(1).berat());
//            child.add(dataset_normal.get(1).tinggi());
//
//            centroid.add(child);
//            K_centroid.add(child);
//
//            child = new ArrayList<Double>();
//
//            child.add(dataset_normal.get(7).usia());
//            child.add(dataset_normal.get(7).berat());
//            child.add(dataset_normal.get(7).tinggi());
//
//            centroid.add(child);
//            K_centroid.add(child);
//
//        } else {
        do {

            Random random = new Random();

            random_int = new ArrayList<Integer>();

            centroid.clear();

            if (k == cluster) {

                K_centroid.clear();
            }

            for (int i = 0; i < k; i++) {

                ArrayList<Double> child = new ArrayList<Double>();

                int value = random.nextInt(dataset_normal.size());

                random_int.add(value);

                child.add(dataset_normal.get(value).usia());
                child.add(dataset_normal.get(value).berat());
                child.add(dataset_normal.get(value).tinggi());

                centroid.add(child);

                if (k == cluster) {

                    data_sebagai_centroid = random_int;
                    K_centroid.add(child);
                }
            }

        } while (!check_generated());

//        }
    }

    private boolean check_generated() {

        boolean status = true;

        for (int i = 0; i < random_int.size(); i++) {

            int temp = random_int.get(i);

            for (int j = 0; j < random_int.size(); j++) {

                if (i != j) {

                    if (temp == random_int.get(j)) {

                        status = false;
                        break;
                    }
                }
            }
        }

        return status;
    }

    private Double kmeans(boolean state, int k) {

        Double sse = 0.0;

        eclud_centroid = new ecludean_distance(dataset_normal, centroid);

        if (state) {

            means = new kmeans(eclud_centroid.result(), dataset_normal, true);

        } else {

            means = new kmeans(eclud_centroid.result(), dataset_normal, false);
        }

        hasil_kmeans = means.hasil_jarak();
        hasil_index = means.hasil_index();
        hasil_min = means.hasil_min();

        if (k == cluster) {

            K_index = new ArrayList<ArrayList<Integer>>(means.hasil_index());
            K_hasil_min = new ArrayList<Double>(means.hasil_min());
            K_kmeans = new ArrayList<ArrayList<Double>>(means.hasil_jarak());
            K_centroid_baru = means.hasil_kmeans();
        }

        SSE hsse = new SSE(hasil_index, hasil_kmeans);

        sse = hsse.hasil_sse();

        return sse;
    }

    private void update_data() {

        for (int i = 0; i < K_index.size(); i++) {

            for (int j = 0; j < K_index.get(i).size(); j++) {

                if (cluster == 2) {

                    dataset.get(K_index.get(i).get(j)).update_kategori(kategori_status_2[i]);
                }

                if (cluster == 3) {

                    dataset.get(K_index.get(i).get(j)).update_kategori(kategori_status_3[i]);
                }

                if (cluster == 4) {

                    dataset.get(K_index.get(i).get(j)).update_kategori(kategori_status_4[i]);
                }

                if (cluster == 5) {

                    dataset.get(K_index.get(i).get(j)).update_kategori(kategori_status_5[i]);
                }

                if (cluster == 6) {

                    dataset.get(K_index.get(i).get(j)).update_kategori(kategori_status_6[i]);
                }
            }
        }
    }

    private void gambar_chart() {

        XY_Chart chart = new XY_Chart(hasil_SSE);

        javax.swing.JScrollPane pane = new javax.swing.JScrollPane();

        pane.setViewportView(chart.hasil_chart());

        tabContainer.addTab("Chart", pane);
    }

    private void update_tabel_data() {

        DefaultTableModel model = (DefaultTableModel) mentah_Tabel.getModel();

        for (int i = 0; i < dataset.size(); i++) {

            model.addRow(new Object[]{dataset.get(i).nama(), dataset.get(i).usia(), dataset.get(i).berat(), dataset.get(i).tinggi(), dataset.get(i).kategori()});
        }

        mentah_Tabel.setModel(model);
    }

    private void update_tabel_normal() {

        DefaultTableModel model = (DefaultTableModel) normal_Tabel.getModel();

        for (int i = 0; i < dataset_normal.size(); i++) {

            model.addRow(new Object[]{dataset_normal.get(i).nama(), dataset_normal.get(i).usia(), dataset_normal.get(i).berat(), dataset_normal.get(i).tinggi()});
        }

        normal_Tabel.setModel(model);
    }

    private void update_tabel_centroid() {

        DefaultTableModel model = (DefaultTableModel) linkage_Tabel.getModel();

        model.addRow(new Object[]{"CENTROID : "});
        model.addRow(new Object[]{});

        ArrayList<ArrayList<String>> temp = new ArrayList<ArrayList<String>>();

        for (int i = 0; i < K_centroid.size(); i++) {

            int val = i + 1;

            ArrayList<String> child = new ArrayList<String>();

            child.add("C " + val);

            for (int j = 0; j < K_centroid.get(i).size(); j++) {

                child.add(K_centroid.get(i).get(j).toString());
            }

            temp.add(child);
        }

        for (int i = 0; i < temp.size(); i++) {

            model.addRow(temp.get(i).toArray());
        }
    }

    private void update_tabel_sse() {

        DefaultTableModel model = (DefaultTableModel) linkage_Tabel.getModel();

        model.addRow(new Object[]{});

        model.addRow(new Object[]{"Nilai SSE : "});

        model.addRow(new Object[]{});

        ArrayList<ArrayList<String>> temp = new ArrayList<ArrayList<String>>();

        for (int i = 0; i < hasil_SSE.size(); i++) {

            int val = i + 2;

            ArrayList<String> child = new ArrayList<String>();

            child.add("K sama dengan " + val);

            child.add(hasil_SSE.get(i).toString());

            temp.add(child);
        }

        for (int i = 0; i < temp.size(); i++) {

            model.addRow(temp.get(i).toArray());
        }

        model.addRow(new Object[]{});

        model.addRow(new Object[]{"Jarak Antar Centroid : ", jarak_centroid});

        model.addRow(new Object[]{});
    }

    private void radio3ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radio3ItemStateChanged
        // TODO add your handling code here:
        int state = evt.getStateChange();

        if (state == java.awt.event.ItemEvent.SELECTED) {

            cluster = Integer.parseInt(radio3.getText());
        }
    }//GEN-LAST:event_radio3ItemStateChanged

    private void radio4ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radio4ItemStateChanged
        // TODO add your handling code here:

        int state = evt.getStateChange();

        if (state == java.awt.event.ItemEvent.SELECTED) {

            cluster = Integer.parseInt(radio4.getText());
        }
    }//GEN-LAST:event_radio4ItemStateChanged

    private void radio5ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radio5ItemStateChanged
        // TODO add your handling code here:

        int state = evt.getStateChange();

        if (state == java.awt.event.ItemEvent.SELECTED) {

            cluster = Integer.parseInt(radio5.getText());
        }
    }//GEN-LAST:event_radio5ItemStateChanged

    private void radio6ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radio6ItemStateChanged
        // TODO add your handling code here:

        int state = evt.getStateChange();

        if (state == java.awt.event.ItemEvent.SELECTED) {

            cluster = Integer.parseInt(radio6.getText());
        }
    }//GEN-LAST:event_radio6ItemStateChanged

    private void radio2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radio2ItemStateChanged
        // TODO add your handling code here:

        int state = evt.getStateChange();

        if (state == java.awt.event.ItemEvent.SELECTED) {

            cluster = Integer.parseInt(radio2.getText());
        }
    }//GEN-LAST:event_radio2ItemStateChanged

    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(kategoristatusgizi.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(kategoristatusgizi.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(kategoristatusgizi.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(kategoristatusgizi.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new kategoristatusgizi().setVisible(true);
            }
        });
    }

    private String[] kategori_status_2 = {"Kurang", "Normal"};
    private String[] kategori_status_3 = {"Kurang", "Normal", "Lebih"};
    private String[] kategori_status_4 = {"Buruk", "Kurang", "Normal", "Lebih"};
    private String[] kategori_status_5 = {"Buruk", "Kurang", "Normal", "Lebih", "Obesitas"};
    private String[] kategori_status_6 = {"Sangat Buruk", "Buruk", "Kurang", "Normal", "Lebih", "Obesitas"};

    private kmeans means;
    private ecludean_distance eclud_centroid;
    private ecludean_centroid centroid_ecludean;
    private Double jarak_centroid;
    private ArrayList<ArrayList<Integer>> index, hasil_index, K_index;
    private ArrayList<ArrayList<Double>> jarak, centroid, hasil_kmeans, K_linkage, K_centroid, K_kmeans, K_centroid_baru;
    private ArrayList<datamodel> dataset, dataset_normal;
    private ArrayList<Double> hasil_SSE, hasil_min, K_hasil_min;
    private ArrayList<Integer> random_int, data_sebagai_centroid;
    private int cluster = 3;

    private File file;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bukaButton;
    private javax.swing.JLabel bukaLabel;
    private javax.swing.JPanel bukaPanel;
    private javax.swing.ButtonGroup buttonGroup;
    private javax.swing.JLabel jumlahLabel;
    private javax.swing.JPanel jumlahPanel;
    private javax.swing.JLabel label_data_mentah;
    private javax.swing.JLabel label_data_normalisasi;
    private javax.swing.JScrollPane linkageScroller;
    private javax.swing.JTable linkage_Tabel;
    private javax.swing.JTextField lokasiField;
    private javax.swing.JScrollPane mentahScroller;
    private javax.swing.JTable mentah_Tabel;
    private javax.swing.JButton mulaiButton;
    private javax.swing.JScrollPane normalScroller;
    private javax.swing.JTable normal_Tabel;
    private javax.swing.JRadioButton radio2;
    private javax.swing.JRadioButton radio3;
    private javax.swing.JRadioButton radio4;
    private javax.swing.JRadioButton radio5;
    private javax.swing.JRadioButton radio6;
    private javax.swing.JSeparator sep1;
    private javax.swing.JSeparator sep2;
    private javax.swing.JSeparator sep3;
    private javax.swing.JSeparator sep4;
    private javax.swing.JTabbedPane tabContainer;
    // End of variables declaration//GEN-END:variables

}
