package kategoristatusgizi;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class SSE {

    private ArrayList<ArrayList<Integer>> index;
    private ArrayList<ArrayList<Double>> kmeans;

    private Double hasil;

    public SSE(ArrayList<ArrayList<Integer>> index, ArrayList<ArrayList<Double>> kmeans) {

        hasil = 0.0;

        this.index = index;
        this.kmeans = kmeans;

        SSE();
    }

    private Double average(int cluster) {

        Double res = 0.0;

        for (int i = 0; i < index.get(cluster).size(); i++) {

            res = res + kmeans.get(cluster).get(index.get(cluster).get(i));
        }

        res = BigDecimal.valueOf(res / index.get(cluster).size()).setScale(5, RoundingMode.HALF_UP).doubleValue();

        return res;
    }

    private void SSE() {

        Double avg;
        Double temp;

        for (int i = 0; i < index.size(); i++) {

            temp = 0.0;
            avg = average(i);

            for (int j = 0; j < index.get(i).size(); j++) {

                temp = temp + (Math.pow(kmeans.get(i).get(index.get(i).get(j)) - avg, 2));
                temp = BigDecimal.valueOf(temp).setScale(5, RoundingMode.HALF_UP).doubleValue();
            }

            hasil = hasil + temp;
        }
    }

    public Double hasil_sse() {

        return hasil;
    }
}
