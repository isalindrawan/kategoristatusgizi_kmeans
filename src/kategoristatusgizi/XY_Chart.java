package kategoristatusgizi;

import java.awt.Color;
import java.awt.BasicStroke;
import java.util.ArrayList;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;

public class XY_Chart {

    private final ChartPanel chartPanel;
    private ArrayList<Double> setdata;

    public XY_Chart(ArrayList<Double> data) {

        this.setdata = data;

        JFreeChart xylineChart = ChartFactory.createXYLineChart(
                "K-Means",
                "Cluster",
                "Nilai SSE",
                createDataset(),
                PlotOrientation.VERTICAL,
                true, true, false);

        chartPanel = new ChartPanel(xylineChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 300));
        final XYPlot plot = xylineChart.getXYPlot();

        NumberAxis domain = (NumberAxis) plot.getDomainAxis();
        domain.setRange(3, 5);
        domain.setTickUnit(new NumberTickUnit(1));
        domain.setVerticalTickLabels(true);

        NumberAxis range = (NumberAxis) plot.getRangeAxis();
        range.setRange(0.0, 0.1);
        range.setTickUnit(new NumberTickUnit(0.01));

        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesPaint(0, Color.BLUE);
        renderer.setSeriesStroke(0, new BasicStroke(2.0f));
        plot.setRenderer(renderer);

    }

    private XYDataset createDataset() {

        final XYSeries data = new XYSeries("Nilai SSE");

        data.add(2, setdata.get(0));
        data.add(3, setdata.get(1));
        data.add(4, setdata.get(2));
        data.add(5, setdata.get(3));
        data.add(6, setdata.get(4));

        final XYSeriesCollection dataset = new XYSeriesCollection();

        dataset.addSeries(data);

        return dataset;
    }

    public ChartPanel hasil_chart() {

        return chartPanel;
    }
}
