package kategoristatusgizi;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class kmeans {

    private ArrayList<datamodel> dataset;
    private ArrayList<ArrayList<Double>> jarak, new_centroid, centroid_sebelum, sort_min, sort_min_sebelum;
    private ArrayList<Integer> index, index_sebelum;
    private ArrayList<Double> hasil_min, hasil_min_sebelum;
    private ArrayList<ArrayList<Integer>> sort_index, sort_index_sebelum;
    private int counter;
    private boolean state, loop;

    public kmeans(ArrayList<ArrayList<Double>> jarak, ArrayList<datamodel> dataset, boolean state) {

        index = new ArrayList<Integer>();
        sort_index = new ArrayList<ArrayList<Integer>>();
        new_centroid = new ArrayList<ArrayList<Double>>();

        counter = 0;
        loop = true;

        this.state = state;
        this.jarak = jarak;
        this.dataset = dataset;

//        if (state) {
//
//            System.out.println("Jarak Awal : " + jarak);
//        }
        while (loop) {

//            if (state) {
//
//                System.out.println("Iterasi : " + counter);
//            }
            cari_minimum();
            sort_centroid();
            new_centroid();

            if (counter > 0) {

                if (cek_centroid()) {

                    break;

                } else {

                    loop = true;
                }
            }

            ecludean_distance dist = new ecludean_distance(dataset, new_centroid);

//            if (state) {
//                System.out.println("Centroid Baru : " + new_centroid);
//            }
            this.jarak = dist.result();

//            if (state) {
//
//                System.out.println("Jarak Baru : " + jarak);
//            }
            counter++;
        }
    }

    private void cari_minimum() {

        int temp;

        if (counter > 0) {

            index_sebelum = new ArrayList<Integer>(index);
            hasil_min_sebelum = new ArrayList<Double>(hasil_min);
        }

        index = null;
        index = new ArrayList<Integer>();
        hasil_min = new ArrayList<Double>();

        for (int i = 0; i < dataset.size(); i++) {

            Double min = jarak.get(0).get(i);
            temp = 0;

//            if (state) {
//                System.out.println("ukuran jarak : " + jarak.size());
//            }
            for (int j = 0; j < jarak.size(); j++) {

                int compare = Double.compare(jarak.get(j).get(i), min);

//                if (state) {
//                    System.out.println("Compare : " + jarak.get(j).get(i) + " dan " + min);
//                }
                if (compare < 0 || compare == 0) {

                    min = jarak.get(j).get(i);
                    temp = j;
                }
            }

//            if (state) {
//                System.out.println("kecil : " + min);
//            }
            index.add(temp);
            hasil_min.add(min);
        }

//        if (state) {
//            System.out.println("Index awal : " + index_sebelum);
//            System.out.println("Index awal : " + index);
//        }
    }

    private void sort_centroid() {

        if (sort_index.size() != 0) {

            sort_index_sebelum = new ArrayList<ArrayList<Integer>>(sort_index);
            sort_min_sebelum = new ArrayList<ArrayList<Double>>(sort_min);
        }

        sort_index = new ArrayList<ArrayList<Integer>>();
        sort_min = new ArrayList<ArrayList<Double>>();

        for (int i = 0; i < jarak.size(); i++) {

            ArrayList<Integer> temp = new ArrayList<Integer>();
            ArrayList<Double> temp_min = new ArrayList<Double>();

            for (int j = 0; j < index.size(); j++) {

                if (index.get(j) == i) {

                    temp.add(j);
                    temp_min.add(hasil_min.get(j));
                }
            }

            sort_index.add(temp);
            sort_min.add(temp_min);
        }

//        if (state) {
//
//            System.out.println("Setelah di sortir  : " + sort_index);
//        }
    }

    private void new_centroid() {

        if (new_centroid.size() != 0) {

            centroid_sebelum = new ArrayList<ArrayList<Double>>(new_centroid);
        }

        new_centroid = new ArrayList<ArrayList<Double>>();

        for (int i = 0; i < sort_index.size(); i++) {

            ArrayList<Double> child = new ArrayList<Double>();

            if (sort_index.get(i).size() == 1) {

                child.add(dataset.get(sort_index.get(i).get(0)).usia());
                child.add(dataset.get(sort_index.get(i).get(0)).berat());
                child.add(dataset.get(sort_index.get(i).get(0)).tinggi());

            } else {

                Double usia = 0.0;
                Double berat = 0.0;
                Double tinggi = 0.0;

                for (int j = 0; j < sort_index.get(i).size(); j++) {

                    usia = usia + dataset.get(sort_index.get(i).get(j)).usia();
                    berat = berat + dataset.get(sort_index.get(i).get(j)).berat();
                    tinggi = tinggi + dataset.get(sort_index.get(i).get(j)).tinggi();
                }

                usia = BigDecimal.valueOf(usia / sort_index.get(i).size()).setScale(5, RoundingMode.HALF_UP).doubleValue();
                berat = BigDecimal.valueOf(berat / sort_index.get(i).size()).setScale(5, RoundingMode.HALF_UP).doubleValue();
                tinggi = BigDecimal.valueOf(tinggi / sort_index.get(i).size()).setScale(5, RoundingMode.HALF_UP).doubleValue();

                child.add(usia);
                child.add(berat);
                child.add(tinggi);
            }

            new_centroid.add(child);
        }

//        if (state) {
//
//            System.out.println("Centroid barul : " + new_centroid);
//        }
    }

    private boolean cek_centroid() {

        boolean status = true;

        for (int i = 0; i < index.size(); i++) {

            if (index.get(i) != index_sebelum.get(i)) {

                status = false;

                break;
            }
        }

        return status;
    }

    public ArrayList<ArrayList<Integer>> hasil_index() {

        return sort_index;
    }

    public ArrayList<ArrayList<Double>> hasil_kmeans() {

        return new_centroid;
    }

    public ArrayList<Double> hasil_min() {

        return hasil_min;
    }

    public ArrayList<ArrayList<Double>> hasil_jarak() {

        return jarak;
    }
}
