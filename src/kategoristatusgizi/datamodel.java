package kategoristatusgizi;

public class datamodel {

    private String nama, kategori;
    private Double usia, berat, tinggi;

    public datamodel(String nama, Double usia, Double berat, Double tinggi) {

        this.nama = nama;
        this.usia = usia;
        this.berat = berat;
        this.tinggi = tinggi;
    }

    public String nama() {

        return nama;
    }

    public Double usia() {

        return usia;
    }

    public Double berat() {

        return berat;
    }

    public Double tinggi() {

        return tinggi;
    }

    public String kategori() {

        return kategori;
    }

    public void update_kategori(String kategori) {

        this.kategori = kategori;
    }
}
