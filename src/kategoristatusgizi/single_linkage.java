package kategoristatusgizi;

import java.util.ArrayList;

public class single_linkage {

    private ArrayList<ArrayList<Double>> data, data_iterasi;
    private ArrayList<Double> nilai_baru;
    private ArrayList<ArrayList<Integer>> index;
    private Double minimum;
    private int baris, kolom, k;

    public single_linkage(ArrayList<ArrayList<Double>> data, int k) {

        this.data = data;
        this.data_iterasi = data;
        this.k = k;

        indexing();

        linkage();
    }

    private void linkage() {

        for (int i = 0; i < data.size(); i++) {

            if (data_iterasi.size() == k) {

                break;
            }

            ArrayList<ArrayList<Double>> result = new ArrayList<ArrayList<Double>>();

            minimum();

            update_index(i);

            result = pre_update();

            nilai_baru = new ArrayList<Double>();

            for (int j = 0; j < index.size(); j++) {

                if (j != 0) {

                    Double min_helper = data.get(index.get(0).get(0)).get(index.get(j).get(0));

                    for (int k = 0; k < index.get(0).size(); k++) {

                        for (int l = 0; l < index.get(j).size(); l++) {

                            int komparasi = Double.compare(data.get(index.get(0).get(k)).get(index.get(j).get(l)), min_helper);

                            if (komparasi < 0) {

                                min_helper = data.get(index.get(0).get(k)).get(index.get(j).get(l));
                            }
                        }
                    }

                    nilai_baru.add(min_helper);
                }
            }

            nilai_baru.add(0, 0.0);

            update(result);
        }
    }

    private void indexing() {

        ArrayList<Integer> child;

        index = new ArrayList<ArrayList<Integer>>();

        for (int i = 0; i < data.size(); i++) {

            child = new ArrayList<Integer>();
            child.add(i);
            index.add(child);
        }
    }

    private void minimum() {

        for (int i = 0; i < data_iterasi.size(); i++) {

            if (data_iterasi.get(0).get(i) != 0.0) {

                minimum = data_iterasi.get(0).get(i);
                baris = 0;
                kolom = i;

                break;
            }
        }

        for (int i = 0; i < data_iterasi.size(); i++) {

            for (int j = 0; j < data_iterasi.get(i).size(); j++) {

                if (data_iterasi.get(i).get(j) != 0.0) {

                    int komparasi = Double.compare(data_iterasi.get(i).get(j), minimum);

                    if (komparasi < 0) {

                        minimum = data_iterasi.get(i).get(j);
                        baris = i;
                        kolom = j;
                    }
                }
            }
        }
    }

    private ArrayList<ArrayList<Double>> pre_update() {

        ArrayList<Double> child;
        ArrayList<ArrayList<Double>> result;

        result = new ArrayList<ArrayList<Double>>();

        for (int i = 0; i < data_iterasi.size(); i++) {

            child = new ArrayList<Double>();

            for (int j = 0; j < data_iterasi.size(); j++) {

                if (i != baris && j != kolom && i != kolom && j != baris) {

                    child.add(data_iterasi.get(i).get(j));
                }
            }

            if (!child.isEmpty()) {

                result.add(child);
            }
        }

        return result;
    }

    private void update(ArrayList<ArrayList<Double>> result) {

        int loop = result.size();

        for (int i = 0; i < loop; i++) {

            int count = i + 1;

            result.get(i).add(0, nilai_baru.get(count));
        }

        result.add(0, nilai_baru);

        data_iterasi = result;
    }

    private void update_index(int data_iterasi_ke) {

        ArrayList<Integer> temp = new ArrayList<Integer>(index.get(baris));

        for (int i = 0; i < index.get(kolom).size(); i++) {

            temp.add(index.get(kolom).get(i));
        }

        if (kolom > baris) {

            index.remove(kolom);
            index.remove(baris);

        } else {

            index.remove(baris);
            index.remove(kolom);
        }

        index.add(0, temp);
    }

    public ArrayList<ArrayList<Integer>> index() {

        return index;
    }

    public ArrayList<ArrayList<Double>> result() {

        return data_iterasi;
    }
}
